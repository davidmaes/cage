// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        */*
// @grant        all
// ==/UserScript==

(function() {
    let slides = [
        {
            p: "Let's try that again",
            img: "https://timedotcom.files.wordpress.com/2015/07/nicolas-cage1.jpg",
        },
        {
            p: "Hi, I'm Nicholas Cage!",
            img: "https://timedotcom.files.wordpress.com/2015/07/nicolas-cage1.jpg",
        },
        {
            p: "You might know me from movies such as Lord of war, Gone in 60 seconds and Face/Off.",
            img: "https://images-na.ssl-images-amazon.com/images/M/MV5BMTU5NjQ2NzQxMl5BMl5BanBnXkFtZTgwNDk0MjE3MjI@._CR270,136,1026,1026_UX402_UY402._SY201_SX201_AL_.jpg",
        },
        {
            p: "I've noticed that you have problems with <u>LOCKING YOUR COMPUTER</u>",
            img: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Nicolas_Cage_Deauville_2013_2.jpg/220px-Nicolas_Cage_Deauville_2013_2.jpg",
        },
        {
            p: "So, I'm going to help you through the problems you're having.",
            img: "https://static.squarespace.com/static/51b3dc8ee4b051b96ceb10de/51ce6099e4b0d911b4489b79/51ce6189e4b0d911b4497679/1357586529757/1000w/cagebodyface.jpg",
        },
        {
            p: "Ain't I the nice guy!",
            img: "https://timedotcom.files.wordpress.com/2015/07/nicolas-cage1.jpg",
        },
        {
            p: "So here is how you do it. You can use one hand, two hands, one hand and a foot",
            img: "http://it.emory.edu/it_images/security/winlock.jpg",
        },
        {
            p: "Whatever floats your goat really",
            img: "http://img.memerial.net/6175/giving-the-world-a-haircut.jpg",
        },
        {
            p: "Just press these two simultaneously, and you're good to go",
            img: "http://it.emory.edu/it_images/security/winlock.jpg",
        },
        {
            p: "Good?.. Good! carry on",
            img: "http://4.bp.blogspot.com/_0f7Muu8izqA/SwVgMrs-1LI/AAAAAAAAA6Y/XNh9ilSY2-M/s1600/Maverick.jpg",
        },
    ];

    let i = 0;
    let popup = null;
    let header = null;
    let close = null;
    let p = null;
    let img = null;
    let filter = null;

    function onCageClick()
    {
        i++;

        if(!slides[i]) {
            document.body.removeChild(popup);
            document.body.removeChild(filter);
            return;
        }

        p.innerHTML = slides[i].p;
        img.setAttribute("src", slides[i].img);



    }

    function onCloseCage(event)
    {
        i = -1;
        p.innerHTML = "Ha ha ha, you think that would work? You have no power here!";
        img.setAttribute("src", "https://compote.slate.com/images/ec535f97-aa93-421a-ab3e-2e7af4fa9d2f.jpg");
        event.stopPropagation();
    }

    function iNeedTheCageTutorial()
    {
        filter = document.createElement("div");
        filter.setAttribute("style", "z-index: 99999998; left:0; right:0; top:0; bottom:0; position:fixed; background-color: #777;");

        popup = document.createElement("div");
        popup.setAttribute('style', 'z-index: 99999999; text-align:center; top: 250px; left:0; right:0; margin-left: auto; margin-right: auto; position: fixed; background-color: #333333; color: #eeeeee !important; cursor: pointer; width: 800px; border: 1px solid #eeeeee');
        popup.addEventListener('click', onCageClick);

        header = document.createElement("div");
        header.setAttribute("style", "text-align: right");
        close = document.createElement("img");
        close.setAttribute("src", "http://download.seaicons.com/icons/danieledesantis/playstation-flat/512/playstation-cross-icon.png");
        close.setAttribute("style", "display: inline; width: 30px; height: 30px; margin: 10px;");
        close.addEventListener("click", onCloseCage);

        img = document.createElement("img");
        img.setAttribute("src", "");
        img.setAttribute("alt", "");
        img.setAttribute("style", "display: inline; margin: 10px auto; border: 1px solid #eee; width: auto; height: 300px");

        header.appendChild(close);

        p = document.createElement("h2");
        p .setAttribute("style", "color: #eee!important; padding: 20px; font-size: 32px; font-family: Impact, Helvetica, sans-serif;");

        popup.appendChild(header);
        popup.appendChild(img);
        popup.appendChild(p);

        document.body.appendChild(filter);
        document.body.appendChild(popup);

        onCageClick();
    }





    iNeedTheCageTutorial();
})();